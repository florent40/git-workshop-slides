---
title: <div class="text-lime-300"><ph-tree /> Worktree</div>

---

<h1 class="text-lime-300"><ph-tree /> Worktree</h1>
<br/>
You are working on your own feature branch. And then, you need to do something else on another branch.

You'll probably do the following

- `git stash`
- `git checkout` the other branch, 
- conclude your work
- `git checkout` again your working branch,
- `git stash pop` to get back your work

It's call `stash and checkout` flow

git worktree will allow you to checkout out more than one branch at a time.

<!-- --- -->
<!-- hideInToc: true -->
<!-- --- -->
<!---->
<!---->
<!-- <h1 class="text-lime-300"><ph-tree /> Worktree</h1> -->
<!-- <br/> -->
<!---->
<!-- You’re working on a project inside the main branch, but need to test and approve changes applied in a feature branch.  -->
<!---->
<!-- With Git worktree, you simply tell Git to checkout the feature branch in a different directory, outside your present working directory and change to that directory. -->
<!-- From here, you can do your work and change back to the original directory to find all your work in progress awaiting you, just as you left it.  -->
<!---->
<!-- Worktree will shared the same git working repository, it mean that if you `git pull main` in any worktree, the pull will also be applied for the other worktrees. -->
<!---->
<!-- You still need to do `npm install` and copies ignored files (like .env). But you can still switch to another branch from a worktrees -->
<!-- <br/><small>(except taht you cannot have twice the same branch acros all worktrees)</small> -->
<!---->

---
hideInToc: true
---

<h1 class="text-lime-300"><ph-tree /> Worktree</h1>

Commands

- Add worktree
```shell
git worktree add -b <new_branch> <worktree_folder> <git_ref>
# or
git worktree add <existing_branch> <worktree_folder>
```

- List worktrees
```shell
git worktree list --verbose
```

- Remove worktree
```shell
git worktree remove <worktree_folder>
# or
git worktree remove -f <worktree_folder>
```

- Cleanup stale worktree
```shell
git worktree prune
```

