---
title: <div class="text-yellow-300"><fluent-patch-20-filled /> Patch</div>
level: 1  
---

<h1 class="text-yellow-300">
<fluent-patch-20-filled /> Patch
</h1>
<br/>

<p>
Patches are an alternative way to exchange code changes.
By creating a patch, you can essentially "export" one or more commits into plain text files, which you can then easily send to someone else for integration.
</p>
<p>
With the widespread use of remote repositories, forks, and Pull Requests, exchanging code via patches isn't very common anymore
</p>


> Many Git projects, including Git itself, are entirely maintained over mailing lists.



---
hideInToc: true
---

<h1 class="text-yellow-300">
<fluent-patch-20-filled /> Patch
</h1>


- Create patch for x commit(s)
```shell {0|1|2|3|4|all}
git format-patch -10 # last 10 commits
git format-patch -3 HEAD # last 3 commits starting from HEAD
git format-patch cd2a59c^..0dd8f6b # between 2 specific commits
git diff > my_custom_patch_file.patch
```

- Create patch files for all new commits in `my_branch` branch
```shell {0|all}
git format-patch my_branch
```

- Generating files
```shell {0|1|2|all}
git format-patch -3 --stdout > multi_commit.patch # create single patch file
git format-patch -3 -o my_folder # create multiple patch files into my_folder
```


---
hideInToc: true
---

<h1 class="text-yellow-300">
<fluent-patch-20-filled /> Patch
</h1>

Applying patch:
- Checkout (or switch to) the associated commit or branch you want the patch applied to.
- Run the command:
```shell
git apply my_patch_file.patch
```

<br/>

To check all diffs that patch brings
```shell {0|all}
git apply --stat my_patch_file.patch # do not apply patch
```

Check patch before actually applying it
```shell {0|all}
git apply --check my_patch_file.patch # do not apply patch
```

