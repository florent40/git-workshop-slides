---
title: <div class="text-purple-300"><material-symbols-tips-and-updates /> Tips & tricks</div>
level: 1
---

<h1 class="text-purple-300"><material-symbols-tips-and-updates /> Tips & tricks</h1>
<br/>
<p>You just configured git with your GPG key and want to sign your previous commits on your branch ?</p>
<br/>

- Rewrite previous commits 
```shell
git rebase -i HEAD~N -x "git commit --amend --reset-author --no-edit"
```

<small>Replace `HEAD~N` with the reference until where you want to rewrite your commits. This can be a hash, HEAD~4, a branch name, ...</small>

<br/>

- Update last commit only 
```shell
git commit --amend --reset-author --no-edit
```


---
hideInToc: true
---
<h1 class="text-purple-300"><material-symbols-tips-and-updates /> Tips & tricks</h1>

<h2>How to unstage files with command line?</h2>
```shell
git restore --stage <file>...
```

If `<file>` is omitted, all staged files will be unstage.



---
hideInToc: true
---
<h1 class="text-purple-300"><material-symbols-tips-and-updates /> Tips & tricks</h1>

<h2>Usefull aliases</h2>
```shell
alias.branches  branch -a
alias.remotes   remote -v
alias.tags      tag -l
alias.aliases   config --get-regexp alias
```

```shell
alias.s       status -s
alias.trash   !git reset --hard HEAD && git clean -fd
alias.amend   commit --amend --reuse-message=HEAD
alias.unstage restore --staged
```

```shell
alias.pl  log --pretty=oneline -n 20 --graph --abbrev-commit
alias.l   !git log --pretty=format:"%ad%C(red)%d %C(yellow)[%h] %C(green)%cn: %Creset%s " --decorate --date=short
```

```shell
alias.wip !f() { git add -A; git rm $(git ls-files --deleted) 2> /dev/null; git commit --no-verify --no-gpg-sign -m "[wip] [skip ci]"; }; f
alias.unwip !f() { git log -n 1 | grep -q -c [wip] && git reset HEAD~1; }; f
```
