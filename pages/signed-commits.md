---
title: <div class="text-slate-400"><material-symbols-lock /> Signed commits</div>
level: 1
---

<h1 class="text-slate-400"><material-symbols-lock /> Signed commits</h1>
<h2>GPG key</h2>
<br/>

- First, create your own GPG key (install GPG for your OS)
```shell
gpg2 --full-gen-key # use default kind of key `RSA and RSA` with `4096-bits key` length
```

- Check your check
```shell
gpg2 --list-secret-keys --keyid-format LONG blabla-pro@comany.com
```

- Copy the GPG key ID (in red)
<pre class="slidev-code language-text"><code>sec   rsa4096/<span class="text-red-500">1EE8FAE695CE6CC5</span> 2023-03-10 [SC]
      8CF59B4C951CDD8CFC60426E1EE8FAE695CE6CC5
uid                 [ultimate] Florent &lt;blabla-pro@company.com&gt;
ssb   rsa4096/0231666077CFE79F 2023-03-10 [E]
</code></pre>

- Copy public key then copy it to Gitlab
```shell
gpg2 --armor --export 1EE8FAE695CE6CC5
```

---
hideInToc: true
---
<h1 class="text-slate-400"><material-symbols-lock /> Signed commits</h1>

<h2>Configuring git locally</h2>

<p>In your work directory, run these commands</p>
```shell
git config --local user.email "blabla-pro@company.com"
git config --local user.name "Florent"
git config --local commit.gpgsign true
git config --local user.signinkey 1EE8FAE695CE6CC5
```

---
hideInToc: true
---
<h1 class="text-slate-400"><material-symbols-lock /> Signed commits</h1>

<h2>Configuring git with `config` file</h2>
<br/>

- Example of conditional configuration

```ini
[includeIf "hasconfig:remote.*.url:git@github.com:castordoc/**"]
  path = $XDG_CONFIG_HOME/git/config-castor
```
or
```ini
[includeIf "gitdir:~/dev/castor/"]
  path = $XDG_CONFIG_HOME/git/config-castor
```

- Content of your conditional configuration file

```ini
[user]
name = Florent
email = blabla-pro@company.com
signingkey = 1EE8FAE695CE6CC5

[commit]
gpgsign = true
```
