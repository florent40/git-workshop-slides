---
title: <div class="text-emerald-300"><tabler-cherry/>  Cherry-pick</div>
level: 1
---

<h1 class="text-emerald-300">
<tabler-cherry/>  Cherry-pick
</h1>
<br/>

<p>
You can pick a commit on one branch to create a copy of the commit with the same changes on another branch.
</p>

<div grid="~ cols-2 gap-2" m="-t-2">
  <img v-click="1" src="/git-cherry-pick-before.png" />
  <arrow v-click-hide="3" v-click="2" x1="550" y1="450" x2="450" y2="330" color="yellow" width="3" arrowSize="1" />
  <img v-click="3" src="/git-cherry-pick.png" />
</div>



---
hideInToc: true
---

<h1 class="text-emerald-300">
<tabler-cherry/>  Cherry-pick
</h1>

Command:
```bash
git cherry-pick {commit-sha}
```

<br/>

Steps:
- Check log to retrieve the SHA of the commit you want to pick
- Switch to the target branch
- Run command
```shell
git cherry-pick {commit-sha}
```

