---
title: <div class="text-red-300"><material-symbols-rebase-edit-outline /> Interactive rebase</div>
level: 1 
---

<h1 class="text-red-300">
<material-symbols-rebase-edit-outline /> Interactive rebase
</h1>

## What is a rebase?

Git rebase will automatically take the commits in your current working branch and apply them to the head of the passed branch.

```shell
git rebase <base>
```

<br/>

`base` could be any kind of git reference:

- sha
- tag
- <i class="text-red-300">branch name</i> (the famous `git rebase master`)
- <i class="text-red-300">relative reference to HEAD</i> (`git rebase HEAD~3`)


---
hideInToc: true
---

<h1 class="text-red-300">
<material-symbols-rebase-edit-outline /> Interactive rebase
</h1>

<br/>

Running `git rebase` with the `-i` flag give you the opportunity to alter individual commits in the process. 
This lets you clean up history by removing, splitting, and altering an existing series of commits

```shell
git rebase --interactive <base>

# shorthand command
git rebase -i <base>
```

---
hideInToc: true
---

<h1 class="text-red-300">
<material-symbols-rebase-edit-outline /> Interactive rebase
</h1>


`git rebase -i HEAD~3`

```git {all|1-3|4-|8|9|10|11|12-15|16|18|29}
pick b152e1c update convention list
pick ef74522 add new-feature service
pick a01410c use new-feature from main code

# Rebase ce8bb80..a01410c onto ce8bb80 (3 commands)
#
# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
# e, edit <commit> = use commit, but stop for amending
# s, squash <commit> = use commit, but meld into previous commit
# f, fixup [-C | -c] <commit> = like "squash" but keep only the previous
#                    commit's log message, unless -C is used, in which case
#                    keep only this commit's message; -c is same as -C but
#                    opens the editor
# x, exec <command> = run command (the rest of the line) using shell
# b, break = stop here (continue rebase later with 'git rebase --continue')
# d, drop <commit> = remove commit
# l, label <label> = label current HEAD with a name
# t, reset <label> = reset HEAD to a label
# m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
#         create a merge commit using the original merge commit's
#         message (or the oneline, if no original merge commit was
#         specified); use -c <commit> to reword the commit message
# u, update-ref <ref> = track a placeholder for the <ref> to be updated
#                       to this position in the new commits. The <ref> is
#                       updated at the end of the rebase
#
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
#
```


<style>
* {
  --slidev-code-line-height: 14px;
}
</style>

---
hideInToc: true
---

<h1 class="text-red-300">
<material-symbols-rebase-edit-outline /> Interactive rebase
</h1>

### Demo using command line
<img src="/full_colored_dark.png" class="h-80 mx-auto"/>
