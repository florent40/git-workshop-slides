---
title: <div class="text-sky-300"><codicon-bracket-error /> Visual conflict resolution</div>
level: 1 
clicks: 4
---

<h1 class="text-sky-300">
<codicon-bracket-error /> Visual conflict resolution
</h1>

### What a conflict looks like?
<div grid="~ cols-2">
  <div order="1">
```text {all|2-10|2,6,10|3-5|7-9|all}
some code here
<<<<<<< HEAD
Convention list:
- order imports
- order type properties
=======
## List of conventions:
- convention #1
- convention #2
>>>>>>> f5a94ce (update README)
some code here
```
  </div>
  <div order="2" ml="8">
    <ul>
      <li v-click-hide="3" v-click="2" >Conflict identifiers</li>
      <li v-click-hide="4" v-click="3">HEAD changes: code that is in the branch we want to merge/rebase</li>
      <li v-click-hide="5" v-click="4">Incoming changes: code you propose for merge/rebase</li>
    </ul>
  </div>


</div>

---
hideInToc: true
clicks: 3
---

<h1 class="text-sky-300">
<codicon-bracket-error /> Visual conflict resolution
</h1>

### How to resolve conflict?
<div grid="~ cols-2">
  <div>
    <div v-click-hide="6">
```text {all|2-4|6-8|0}
<<<<<<< HEAD
Convention list:
- order imports
- order type properties
=======
## List of conventions:
- convention #1
- convention #2
>>>>>>> f5a94ce (update README)
```
</div>
<div v-click="3">
```text {all}
some code here
## Conventions
- convention #1
- order imports
- order type properties
- another convention
- last convention
some code here
```
    </div>
  </div>
 <div ml="8">
    <ul>
      <li v-click-hide="2" v-click="1">Accept all current changes (HEAD)</li>
      <li v-click-hide="3" v-click="2">Accept all incoming changes</li>
      <li v-click-hide="4" v-click="3">Modifiy manually the code</li>
    </ul>
  </div>
</div>

---
hideInToc: true
---

<h1 class="text-sky-300">
<codicon-bracket-error /> Visual conflict resolution
</h1>

### Resolve conflict using VS Code


![](/vscode-merge-conflict.png)
<arrow v-click-hide="2" v-click="1" x1="400" y1="300" x2="230" y2="190" color="red" width="3" arrowSize="1" />
<arrow v-click-hide="3" v-click="2" x1="530" y1="300" x2="360" y2="190" color="red" width="3" arrowSize="1" />
<arrow v-click-hide="4" v-click="3" x1="730" y1="300" x2="560" y2="190" color="red" width="3" arrowSize="1" />
<arrow v-click-hide="5" v-click="4" x1="870" y1="300" x2="700" y2="190" color="red" width="3" arrowSize="1" />


---
hideInToc: true
---

<h1 class="text-sky-300">
<codicon-bracket-error /> Visual conflict resolution
</h1>


### Resolve conflict using command line
<br/>
<div grid="~ cols-2 gap-4">
  <ul>
    <li>Check conflict files with
```bash
git status
```
    </li>
    <li>Update code using your favorite tool</li>
    <li>Finally, finalize conflict resolution with
```bash
git add filename_with_conflict
```
or
```bash
git add .
```
    </li>
  </ul>
<div>

![](/cli-merge-conflict.png)
<br/>
<v-click at="1">

![](/cli-merge-conflict-status.png)

</v-click>
</div>
</div>

---
hideInToc: true
---
<h1 class="text-sky-300">
<codicon-bracket-error /> Visual conflict resolution
</h1>

### Demo with VS Code

<img src="/vscode.png" class="h-60 mx-auto" />
