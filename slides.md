---
theme: default
highlighter: prism
class: text-white
hideInToc: true
drawings: 
  persist: false
---

# <mdi-git /> GIT workshop

---
hideInToc: true
---
# Table of content
<br/>
<br/>
<br/>
<Toc />


---
src: ./pages/cherry-pick.md
---

---
src: ./pages/patch.md
---

---
src: ./pages/conflicts.md
---

---
src: ./pages/rebase-interactive.md
---

---
src: ./pages/worktree.md
---

---
src: ./pages/signed-commits.md
---

---
src: ./pages/tips-and-tricks.md
---
